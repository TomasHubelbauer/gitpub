import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as fs from 'fs-extra';
import * as GitClient from 'isomorphic-git';
import * as path from 'path';
import * as GitServer from 'node-git-server';
import * as FtpServer from 'simple-ftpd';
import { Readable } from 'stream';

if (process.platform === 'win32') {
	console.log('Use WSL for now. See README. Also http://www.michaelfcollins3.me/blog/2012/05/18/implementing-a-git-http-server.html');
}
function getPathForName(name) {
	return path.join('../../data', name);
}

const apiPort = 4000;
const apiServer = express();
const jsonParser = bodyParser.json();

apiServer.use(cors());

apiServer.listen(apiPort, () => {
	console.log(`Listening on ${apiPort} for API…`);
});

apiServer.get('/repos', async (request, response) => {
	const repos = (await fs.readdir('../../data')).map(path => ({ id: path, name: path }));
	response.json({ repos });
});

apiServer.post('/create', jsonParser, async (request, response) => {
	const path = getPathForName(request.body.name);
	await fs.mkdirp(path);
	await GitClient.init({ fs, dir: path });
	response.json({});
});

apiServer.post('/delete', jsonParser, async (request, response) => {
	const path = getPathForName(request.body.id);
	await fs.remove(path);
	response.json({});
});

apiServer.get('/issues/:name', async (request, response) => {
	const { name } = request.params;
	const path = getPathForName(name) + '/todo';
	await fs.ensureDir(path);
	const issues = [];
	for (const p of await fs.readdir(path)) {
		const { atime, ctime } = await fs.stat(path + '/' + p);
		console.log(atime, ctime);
		issues.push({ accessed: atime, changed: ctime, name: p });
	}

	response.json({ name, issues });
});

// TODO: Make this a tree recursively.
apiServer.get('/wikis/:name', async (request, response) => {
	const { name } = request.params;
	const path = getPathForName(name) + '/wiki';
	await fs.ensureDir(path);
	const wikis = [];
	for (const p of await fs.readdir(path)) {
		const { atime, ctime } = await fs.stat(path + '/' + p);
		console.log(atime, ctime);
		wikis.push({ accessed: atime, changed: ctime, name: p });
	}

	response.json({ name, wikis });
});

const gitPort = 7000;
const gitServer = new GitServer('../../data', { autoCreate: false, authenticate: (type, repo, user, next) => next() });

gitServer.on('push', (push) => {
	console.log(`push ${push.repo}/${push.commit} (${push.branch})`);
	push.accept();
});

gitServer.on('fetch', (fetch) => {
	console.log(`fetch ${fetch.commit}`);
	fetch.accept();
});

gitServer.listen(gitPort, {}, () => {
	console.log(`Listening on ${gitPort} for Git…`);
});

// https://github.com/kamicane/simple-ftpd
// Access `ftp://localhost:1337/` in Firefox
const ftpServer = FtpServer({ host: '127.0.0.1', port: 1337, readOnly: false }, session => {
	session.on('pass', (userName, password, cb) => {
		console.log('pass', userName, password)
		if (userName === 'anonymous' && (password == 'mozilla@example.com' /* Firefox */ || password === 'IEUser@')) {
			cb();
		}
	});

	session.on('stat', (pathName, cb) => {
		console.log('stat', pathName);
		if (pathName.endsWith('.txt')) {
			cb(null, { mode: 33286 /* WTF */, size: 0, mtime: Date.now() });
		} else {
			cb(null, { mode: 16822 /* WTF */, size: 0, mtime: Date.now() });
		}
	});

	session.on('readdir', (pathName, cb) => {
		console.log('readdir', pathName);
		cb(null, [ 'a', 'b', 'c', 'a.txt', 'b.txt', 'c.txt' ]);
	});

	session.on('read', (pathName, offset, cb) => {
		console.log('read', pathName, offset);
		const readable = new Readable();
		readable.push('eyyy');
		readable.push(null);
		readable._read = console.log;
		cb(null, readable);
	});

	session.on('write', (pathName, offset, cb) => console.log('write', pathName, offset));

	
	session.on('mkdir', (pathName, cb) => console.log('mkdir', pathName));
	session.on('unlink', (pathName, cb) => console.log('unlink', pathName));
	session.on('remove', (pathName, cb) => console.log('remove', pathName));
	session.on('rename', (fromName, toName, cb) => console.log('rename', fromName, toName));
});
