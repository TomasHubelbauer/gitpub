import { h, mount, patch } from 'petit-dom';
const gitPubUrl = 'http://localhost:4000';

type Params = { [name: string]: string; };
type Repo = { id: string; name: string; };
type Issue = { created: string, accessed: string, name: string; };
type Wiki = { created: string, accessed: string, name: string; };

function makeUrl(pathName: string, params?: Params) {
  const url = new URL(gitPubUrl);
  url.pathname = pathName;
  if (params) {
    for (const param in params) {
      url.searchParams.append(param, params[param]);
    }
  }

  return url.toString();
}

async function clearError<T>(response: Response) {
  const data = await response.json();
  if (data.error) {
    alert(data.error);
    throw new Error(data.error);
  }

  return data as T;
}

async function get<T>(pathName: string, params?: Params) {
  const response = await fetch(makeUrl(pathName, params), { method: 'get', headers: { 'Accept': 'application/json' } });
  return await clearError<T>(response);
}

async function post<T>(pathName: string, body: object, params?: Params) {
  const response = await fetch(makeUrl(pathName, params), { method: 'post', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, body: JSON.stringify(body) });
  return await clearError<T>(response);
}

window.addEventListener('load', async _ => {
  let appNode = <div/>;
  document.body.appendChild(mount(appNode));

  // State:
  let page: 'index' | 'repo' = 'index';
  
  // -> Index page state:
  let repos: Repo[] = [];
  
  // -> Repo page state:
  let repo: Repo;
  // Extend to include loading states.
  let issues: Issue[];
  let wikis: Wiki[];

  renderPage();
  await refresh();

  async function refresh() {
    const data = await get<{ repos: Repo[] }>('/repos');
    repos = data.repos;
    renderPage();
  }

  async function onNewButtonClick() {
    const name = prompt('Name:');
    if (!name) {
      return;
    }

    await post('/create', { name });
    await refresh();
  }

  async function onOpenAClick(event: Event) {
    const id = (event.currentTarget as HTMLButtonElement).dataset['id'];
    repo = repos.find(r => r.id === id);
    page = 'repo';
    issues = [];
    wikis = [];
    renderPage();
    issues = (await get<{ name: string, issues: Issue[] }>('/issues/' + repo.id)).issues;
    wikis = (await get<{ name: string, wikis: Wiki[] }>('/wikis/' + repo.id)).wikis;
    renderPage();
  }

  async function onDeleteButtonClick(event: Event) {
    const id = (event.currentTarget as HTMLButtonElement).dataset['id'];
    const repo = repos.find(r => r.id === id);
    if (confirm(`Are you sure you wnat to delete '${repo.name}'?`)) {
      await post('/delete', { id });
      await refresh();
    }
  }

  function onBackAClick() {
    page = 'index';
    renderPage();
  }


  function renderPage() {
    switch (page) {
      case 'index': return renderIndex();
      case 'repo': return renderRepo();
      default: return renderError();
    }
  }

  function renderError() {
    render(<div>
      <span>Unknown page '{page}'.</span>
    </div>);
  }

  function renderRepo() {
    render(<div>
      <a onclick={onBackAClick}>Back</a>
      <h1>{repo.name}</h1>
      <h2>Clone</h2>
      <p>
        <code>git clone http://localhost:7000/{repo.id}</code>
      </p>
      <h2>Issues</h2>
      <button>New</button>
      {issues.length === 0 ? 'No issues yet' : ''}
      <ul>
        {issues.map(i => <li>{i.name}</li>)}
      </ul>
      <h2>Wikis</h2>
      <button>New</button>
      {wikis.length === 0 ? 'No wikis yet' : ''}
      <ul>
        {wikis.map(w => <li>{w.name}</li>)}
      </ul>
      <h2>Branches</h2>
      TODO: Load branches upon reaching this page (each branch is implicitly a PR/MR and has review UI)
    </div>)
  }

  function renderIndex() {
    render(<div>
      <div>
        <button onclick={onNewButtonClick}>New</button>
      </div>
      {repos.map(repo => <div>
        <a data-id={repo.id} href={'#repo/' + repo.id} onclick={onOpenAClick}>{repo.name}</a>
        <button data-id={repo.id} onclick={onDeleteButtonClick}>Delete</button>
      </div>)}
    </div>);
  }

  function render(node) {
    patch(node, appNode);
    appNode = node;
  }
});
