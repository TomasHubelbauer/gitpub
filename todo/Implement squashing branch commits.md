# Implement squashing branch commits

Useful for when in the app, a user decided to contribute, so forks master, makes changes in the online editor for each file (a commit each time)
and then for review wants the changes to appears as one.
