# Document special directories

GitPub will likely store repository metadata in the repository itself (as opposed to a separate repository).

Issues will be stored in a `todo` directory in the root of the repository (or configurable, maybe `.gitpub/todo`?).

Docs will be stored in a `wiki` directory in the root of the repository (or configurable, maybe `.gitpub/wiki`?).

Reviews will be stored where?

- [ ] Figure how to store reviews out
