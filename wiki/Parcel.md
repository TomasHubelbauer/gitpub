# Parcel

This project uses the Parcel bundler.

## Basic Usage

- `parcel index.html` for develoment watched run
- `parcel build index.html` for production build (in `dist`)
