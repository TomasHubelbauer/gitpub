# GitPub

> An idea.

A like of GitHub and GitLab, but objects like issues, wikis etc. are kept in a
Git directory also, once clone-able and accessible from the same server.

The repository only allows pulling, so backups of the GitDab data can be made
regularly and simply by using Git. The items are modified through the GitDab UI.
That's a design decision for now, in the future, it is possible pushing to the
meta repository will also be allowed, as long as we can made sure the application
will recover okay from broken files and unsuccessful merges and such.

Updated idea:

- There is a web UI like GitHub or GitLab, exactly the same thing
- All backing data are stored in a corresponding directory
- There is a Git server so users can clone locally and push
- There is an also an FTP server mountable as a network location
  - The root directory is a list of directories which correspond to repositories
  - Creating a new root directory makes a new repository
  - Creating files in root is prohibited (although maybe non-repo settings here?)
  - Creating files and directories in the repository directory changes shit in repo, commit through web UI?
  - Maybe just commit with each save? Re-show the auth window for commit message or implicit message?
    - By 10060, 10054, 532, **530**? https://en.wikipedia.org/wiki/List_of_FTP_server_return_codes
- Maybe a real virtual drive for Windows even? https://github.com/billziss-gh/winfsp
- [ ] FTPS

## Running

| Prerequisite | Minimal Version | Optimal Version | Justification   |
|--------------|-----------------|-----------------|-----------------|
| NodeJS       | ?               | 9.4.0           | Runtime         |
| Yarn         | ?               | 1.3.2           | Package Manager |
| Nodemon      | ?               | ?               | Watcher         |

- BE: `cd src/be && bash`, `yarn start` (not `ubuntu` because of [this issue](https://github.com/Microsoft/WSL/issues/3017))
- FE: `cd src/fe && yarn start`

It is recommended to use VS Code and open two integrated terminal windows side-by-side to run this.
